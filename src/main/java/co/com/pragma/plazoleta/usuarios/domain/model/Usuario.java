package co.com.pragma.plazoleta.usuarios.domain.model;

import java.math.BigInteger;
import java.util.Date;

public class Usuario {

    private BigInteger id;
    private String nombre;
    private String apellido;
    private String numeroDocumento;
    private String celular;
    private Date fechaNacimiento;
    private String correo;
    private String clave;
    private Integer idRol;

}
